import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProfileComponent } from './components/profile/profile.component';
import { AccueilComponent } from './components/accueil/accueil.component';
import { ClientsComponent } from './components/clients/clients.component';
import { OperationsComponent } from './components/operations/operations.component';
import { ComptesComponent } from './components/comptes/comptes.component';
import { ClientProfileComponent } from './components/clients/client-profile/client-profile.component';
import { LogsComponent } from './components/logs/logs.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { AddClientComponent } from './components/forms/add-client/add-client.component';
import { MatStepperModule } from '@angular/material/stepper';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatOptionModule } from '@angular/material/core';
import { MatRadioModule } from '@angular/material/radio';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatSelectModule } from '@angular/material/select';
import { MatCardModule } from '@angular/material/card';
import { LoginComponent } from './components/login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AddCompteComponent } from './components/forms/add-compte/add-compte.component';
import { CompteProfileComponent } from './components/clients/compte-profile/compte-profile.component';
import { NavbarHeaderComponent } from './components/navbar-header/navbar-header.component';
import {
  HTTP_INTERCEPTORS,
  HttpClientModule,
  HttpClientXsrfModule,
} from '@angular/common/http';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { PasswordConfirmationComponent } from './components/forms/password-confirmation/password-confirmation.component';
import { Error404Component } from './components/error404/error404.component';
import { SnackBarComponent } from './components/forms/snack-bar/snack-bar.component';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatToolbarModule } from '@angular/material/toolbar';

import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';
import { NgxsModule } from '@ngxs/store';
import { RequestsInterceptor } from './requests.interceptor';
import { AppState } from './states/app.state';
import { PhotoPipe } from './photo.pipe';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { HistoriqueComponent } from './components/historique/historique.component';
import { WebsocketService } from './services/websocket.service';
import { StompRService } from '@stomp/ng2-stompjs';
import { DemandesComponent } from './components/demandes/demandes.component';
import { ForgetPasswordComponent } from './components/forget-password/forget-password.component';

@NgModule({
  declarations: [
    AppComponent,
    ProfileComponent,
    AccueilComponent,
    ClientsComponent,
    OperationsComponent,
    ComptesComponent,
    ClientProfileComponent,
    LogsComponent,
    AddClientComponent,
    LoginComponent,
    AddCompteComponent,
    CompteProfileComponent,
    NavbarHeaderComponent,
    PasswordConfirmationComponent,
    Error404Component,
    SnackBarComponent,
    PhotoPipe,
    DashboardComponent,
    HistoriqueComponent,
    DemandesComponent,
    ForgetPasswordComponent,
  ],
  entryComponents: [AddClientComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatDialogModule,
    MatStepperModule,
    MatFormFieldModule,
    MatDatepickerModule,
    MatCheckboxModule,
    MatOptionModule,
    MatRadioModule,
    MatGridListModule,
    MatSelectModule,
    MatCardModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatInputModule,
    MatIconModule,
    MatSnackBarModule,
    MatToolbarModule,
    HttpClientXsrfModule.withOptions({
      cookieName: 'XSRF-TOKEN',
      headerName: 'X-XSRF-TOKEN',
    }),
    NgxsReduxDevtoolsPluginModule.forRoot(),
    NgxsModule.forRoot([AppState], {
      developmentMode: true,
    }),
  ],
  providers: [
    StompRService,
    WebsocketService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: RequestsInterceptor,
      multi: true,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
