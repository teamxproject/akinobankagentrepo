import { Component, OnInit } from '@angular/core';
import { ClientsService } from '../../../services/clientsServices/clients.service';
import { User } from '../../../models/user';
import { Compte } from '../../../models/compte';
import { ActivatedRoute, Router } from '@angular/router';
import { ComptesService } from '../../../services/comptesService/comptes.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Recharge } from '../../../models/recharge';
import { Virement } from '../../../models/virement';
import { CompteStatus } from '../../../models/enum/compte-status';
import {Select, Store} from '@ngxs/store';
import {SetSelectedClient, SetSelectedCompte} from '../../../actions/clients.actions';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-compte-profile',
  templateUrl: './compte-profile.component.html',
  styleUrls: ['./compte-profile.component.css'],
})
export class CompteProfileComponent implements OnInit {

  @Select(store => store.state.selectedClient)
  client: Observable<User>;
//
  @Select(store => store.state.selectedCompte)
  compte: Observable<Compte>;
  compteFormGroup: FormGroup;
  compteStatusFormGroup: FormGroup;
  virements: Virement[];
  recharges: Recharge[];

  idCompte: number;
  idClient: number;

  constructor(
    private _snackBar: MatSnackBar,
    private clientsService: ClientsService,
    private activatedRoute: ActivatedRoute,
    private comptesService: ComptesService,
    private router: Router,
    private store: Store
  ) {
    console.log(this.activatedRoute.snapshot.url[2].path);
    this.idCompte = this.activatedRoute.snapshot.params.id;
    this.idClient = parseInt(this.activatedRoute.snapshot.url[2].path);
    console.log(this.idCompte);
  }

  ngOnInit(): void {
    this.compteFormGroup = new FormGroup({
      intitule: new FormControl("", Validators.required),
      solde: new FormControl("", Validators.required),
    });
    this.compteStatusFormGroup = new FormGroup({
      statut: new FormControl('', Validators.required),
    });

    this.compteStatusFormGroup.patchValue(this.compte);
    this.store.dispatch(new SetSelectedCompte(this.idCompte));
    this.compte
      .subscribe(data => {
        if (data) {
        this.compteFormGroup.patchValue(data);
        this.compteStatusFormGroup.patchValue(data);

        }
      });

    this.store.dispatch(new SetSelectedClient(this.idClient));



   // this.store.dispatch(new )
 //   this.getCompteClient();

  //  this.getClient();
  }

  openSnackBar(snackBarmessage, snackBarAction) {
    this._snackBar.open(snackBarmessage, snackBarAction, {
      duration: 2000,
    });
  }

  getCompteClient() {
    this.comptesService.getCompte(this.idCompte).subscribe(
      (response) => {
  //      this.compte = <Compte>response;
    //    this.virements = this.compte.virements;
      //  this.recharges = this.compte.recharges;
        console.log(response);

      },
      (error) => {
        console.log(error);
        // this.router.navigateByUrl("/error404");
      }
    );
  }
  getClient() {
    this.clientsService
      .getClientProfile(this.idClient)
      .subscribe((response) => {
       // this.client = <User>response;
      });
  }

  modifyCompteinfo() {
    console.log(this.compteFormGroup.value);
  //  this.compte.intitule = this.compteFormGroup.value.intitule;
   // this.compte.solde = this.compteFormGroup.value.solde;
    this.comptesService.modifyCompte(this.idCompte, this.compte).subscribe(
      (response) => {
        console.log(response);
        this.openSnackBar('La modification est enregistrée', 'undo');
      },
      (error) => {
        console.log(error);
      }
    );
  }

  modifyCompteStatut() {
    console.log(this.compteStatusFormGroup.value);
    //this.compte.statut = this.compteStatusFormGroup.value.statut;
    this.comptesService
      .modifyCompteStatus(
        this.idCompte,
        this.compteStatusFormGroup.value.statut
      )
      .subscribe(
        (response) => {
          console.log(response);
          this.openSnackBar('La modification est enregistrée', 'undo');
        },
        (error) => {
          console.log(error);
        }
      );
  }
}
