import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { AddClientComponent } from '../forms/add-client/add-client.component';
import { ProfileService } from '../../services/profileServices/profile.service';
import { User } from '../../models/user';
import { ClientsService } from '../../services/clientsServices/clients.service';
import { Client } from '../../models/client';
import { Route, Router } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Demande } from '../../models/demande';
import { Store, Select } from '@ngxs/store';
import {
  GetClients,
  SetSelectedClient,
  SuppClient,
} from 'src/app/actions/clients.actions';
import { AppState } from 'src/app/states/app.state';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import {DemandesComponent} from "../demandes/demandes.component";

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.css'],
})
export class ClientsComponent implements OnInit {
  profile: User;

  @Select(AppState.selectClients)
  clients: Observable<User[]>;

  clientsImage: any;
  clientsAvatar: any[];
  client:User[];

  constructor(
    private _snackBar: MatSnackBar,
    private sanitizer: DomSanitizer,
    private dialog: MatDialog,
    private clientsService: ClientsService,
    private profileService: ProfileService,
    private router: Router,
    private store: Store
  ) {}

  ngOnInit(): void {
    // this.clientsService.refreshNeeds$.subscribe(() => {
    //   console.log('Need to refresh clients.');
    //   this.getClients();
    // });
    this.store.dispatch(new GetClients());

    this.clients.subscribe(clients => {
        console.log(clients);
        this.client= clients;
      }
    )}

  openSnackBar(snackBarmessage, snackBarAction) {
    this._snackBar.open(snackBarmessage, snackBarAction, {
      duration: 2000,
    });
  }

  openDialog() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '500px';
    dialogConfig.data = {
      title: 'Ajouter Client',
    };

    this.dialog.open(AddClientComponent, dialogConfig);
  }

  // onDownloadImage(photo){
  //   console.log(photo);
  //   this.clientsService.getClientImage(photo).subscribe(image=>{
  //     // this.createImageFromBlob(image);
  //     let objectURL = URL.createObjectURL(image);
  //     return this.sanitizer.bypassSecurityTrustUrl(objectURL);
  //     // console.log(this.clientsImage.changingThisBreaksApplicationSecurity);
  //   },error => {
  //     console.log(error);
  //   })
  // }

  // createImageFromBlob(image: Blob) {
  //   console.log(image);
  //   let reader1 = new FileReader();
  //   reader1.addEventListener("load", () => {
  //     this.clientsImage =reader1.result;
  //     console.log("hh")
  //   }, false);
  //   if (image) {
  //     reader1.readAsDataURL(image);
  //   }
  // }

  clientProfile(id: number) {
    this.store
      .dispatch(new SetSelectedClient(id))
      .subscribe(() => this.router.navigateByUrl('/accueil/clients/profile/' + id));
  }

  deleteClient(id: number) {
    let conf = confirm('Vous êtes sur ?');
    if (conf) this.store.dispatch(new SuppClient(id));
    //   this.clientsService.deleteClient(id).subscribe(
    //     (response) => {
    //       this.openSnackBar(response, 'undo');
    //       this.getClients();
    //     },
    //     (error) => {
    //       console.log(error);
    //     }
    //   );
    // }
  }

  openDemandes(client) {
    console.log('Open demands');

    // this.salarie.subscribe(salarie =>{
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '500px';
    dialogConfig.data = {
      title: 'Afficher Demandes',
      client :client

    };
    this.dialog.open(DemandesComponent,dialogConfig);

  }
}
