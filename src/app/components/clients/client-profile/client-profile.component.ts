import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ClientsService } from '../../../services/clientsServices/clients.service';
import { User } from '../../../models/user';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AddCompteComponent } from '../../forms/add-compte/add-compte.component';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { Compte } from '../../../models/compte';
import { ClientsComponent } from '../clients.component';
import { DomSanitizer } from '@angular/platform-browser';
import { CompteProfileComponent } from '../compte-profile/compte-profile.component';
import { PasswordConfirmationComponent } from '../../forms/password-confirmation/password-confirmation.component';
import { ComptesService } from '../../../services/comptesService/comptes.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Store, Select } from '@ngxs/store';
import {
  SetSelectedClient,
  AddClient,
  ModClient,
  ModClientContacts,
  SuppCompte,
} from 'src/app/actions/clients.actions';
import { AppState } from 'src/app/states/app.state';
import { Observable, Subscribable, Subscription } from 'rxjs';
import { GetSelectedClientActivities } from 'src/app/actions/activities.actions';
import { Activity } from 'src/app/models/activity';
import { WebsocketService } from 'src/app/services/websocket.service';

@Component({
  selector: 'app-client-profile',
  templateUrl: './client-profile.component.html',
  styleUrls: ['./client-profile.component.css'],
})
export class ClientProfileComponent implements OnInit, OnDestroy {
  idClient: number;
  activitiesExpanded = false;

  @Select(AppState.selectOneClient)
  client: Observable<User>;

  @Select(AppState.selectSelectedClientActivities)
  activities: Observable<Activity[]>;

  websocketSusbscription: Subscription;

  clientProfileFormGroup: FormGroup;
  saved: boolean = false;
  passwordClientFromGroup: FormGroup;
  clientContactFromGroup: FormGroup;
  clientContact: boolean = false;
  profileImage: any;
  hide: true;
  comptes: Compte[];
  clients: ClientsComponent;
  formClientSettings: boolean = false;
  formClientContacts: boolean = false;
  formClientPassword: boolean = false;
  constructor(
    private _snackBar: MatSnackBar,
    private comptesService: ComptesService,
    private sanitizer: DomSanitizer,
    private dialog: MatDialog,
    private activatedRoute: ActivatedRoute,
    private clientsService: ClientsService,
    private router: Router,
    private store: Store,
    private websocketService: WebsocketService
  ) {}

  ngOnInit(): void {
    this.clientProfileFormGroup = new FormGroup({
      nom: new FormControl('', Validators.required),
      prenom: new FormControl('', Validators.required),
      email: new FormControl('', [Validators.required, Validators.email]),
      numeroTelephone: new FormControl('', Validators.required),
    });

    this.clientContactFromGroup = new FormGroup({
      ville: new FormControl('', Validators.required),
      adresse: new FormControl('', Validators.required),
    });

    this.idClient = this.activatedRoute.snapshot.params.id;
    this.store.dispatch(new SetSelectedClient(this.idClient));
    this.client.subscribe((data) => {
      if (data) {
        console.log('Loaded client data', data);
        this.clientProfileFormGroup.patchValue(data);
        this.clientContactFromGroup.patchValue(data);
      }
    });
    console.log(this.activatedRoute.snapshot.params.id);
    if (this.activitiesExpanded) {
      this.websocketSusbscription = this.websocketService
        .getStomp()
        .subscribe('/user/topic/activities.clients.' + this.idClient)
        .subscribe((data) => {
          const activity: Activity = JSON.parse(data.body);
          this.activities.subscribe((data) => data.unshift(activity));
          console.log('Received : ', activity);
        });
    }
    // this.comptesService.refreshNeeds$.subscribe(() => {
    //   this.getClientProfile(this.idClient);
    // });
    // this.getClientProfile(this.idClient);
  }

  ngOnDestroy() {
    this.websocketSusbscription?.unsubscribe();
  }

  openSnackBar(snackBarmessage, snackBarAction) {
    this._snackBar.open(snackBarmessage, snackBarAction, {
      duration: 2000,
    });
  }

  onDownloadImage(photo) {
    console.log(photo);
    this.clientsService.getClientImage(photo).subscribe(
      (image) => {
        let objectURL = URL.createObjectURL(image);
        this.profileImage = this.sanitizer.bypassSecurityTrustUrl(objectURL);
        // console.log(this.clientsImage.changingThisBreaksApplicationSecurity);
      },
      (error) => {
        console.log(error);
      }
    );
  }

  modifyClientProfile() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '500px';
    dialogConfig.data = {
      // client: this.client,
      // form: form,
      title: "Vérifier l'identité de l'agent",
    };
    this.dialog
      .open(PasswordConfirmationComponent, dialogConfig)
      .afterClosed()
      .subscribe((password) => {
        if (password) {
          this.store
            .dispatch(
              new ModClient(this.idClient, {
                user: this.clientProfileFormGroup.value,
                agentPassword: password,
              })
            )
            .subscribe(() => (this.formClientSettings = true));
        }
      });
    //   console.log(this.clientProfileFormGroup.value);
    //   this.client.nom = this.clientProfileFormGroup.value.nom;
    //   this.client.prenom = this.clientProfileFormGroup.value.prenom;
    //   this.client.email = this.clientProfileFormGroup.value.email;
    //   this.client.numeroTelephone = this.clientProfileFormGroup.value.numeroTelephone;
    //   this.verificationDialog(1);
    //   // this.clientsService.modifyClientSettings(this.client).subscribe(response => {
    //   //   console.log(response);
    //   //   this.formClientSettings = true
    //   // },error => {
    //   //   this.formClientSettings = false;
    //   // })
  }

  showActivities() {
    this.store.dispatch(new GetSelectedClientActivities()).subscribe(() => {
      this.activitiesExpanded = true;
      this.websocketSusbscription = this.websocketService
        .getStomp()
        .subscribe('/user/topic/activities.clients.' + this.idClient)
        .subscribe((data) => {
          const activity: Activity = JSON.parse(data.body);
          // this.store.dispatch(new AddActivitiy(activity));
          this.activities.subscribe((data) => [activity, ...data]);
          console.log('Received : ', activity);
        });
    });
  }

  verificationDialog(form) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '500px';
    dialogConfig.data = {
      // client: this.client,
      // form: form,
      title: "Vérifier l'identité de l'agent",
    };
    this.dialog
      .open(PasswordConfirmationComponent, dialogConfig)
      .afterClosed()
      .subscribe((data) => {
        if (data) {
          this.store;
        }
      });
  }

  modifyClientContact() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '500px';
    dialogConfig.data = {
      title: 'Ajouter Compte',
      // client: this.client,
    };
    this.dialog
      .open(PasswordConfirmationComponent, dialogConfig)
      .afterClosed()
      .subscribe((password) => {
        if (password) {
          this.store
            .dispatch(
              new ModClientContacts(this.idClient, {
                user: this.clientContactFromGroup.value,
                agentPassword: password,
              })
            )
            .subscribe(() => (this.formClientContacts = true));
        }
      });
    // this.client.ville = this.clientContactFromGroup.value.ville;
    // this.client.adresse = this.clientContactFromGroup.value.adresse;
    // this.clientsService.modifyClientContacts(this.client).subscribe(
    //   (response) => {
    //     console.log(response);
    //     this.formClientContacts = true;
    //     this.openSnackBar('La modification est enregistrée', 'undo');
    //   },
    //   (error) => {
    //     console.log(error);
    //     this.formClientContacts = false;
    //   }
    // );
  }

  openDialog() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '400px';
    dialogConfig.data = {
      title: 'Ajouter Compte',
      // client: this.client,
    };
    this.dialog.open(AddCompteComponent, dialogConfig);
  }
  onDeleteCompte(numeroCompte: string) {
    let conf = confirm('Vous êtes sur ?');
    if (conf) this.store.dispatch(new SuppCompte(numeroCompte));
    //   this.comptesService.deleteCompte(numeroCompte).subscribe(
    //     (response) => {
    //       this.openSnackBar(response, 'undo');
    //       this.getClientProfile(this.idClient);
    //     },
    //     (error) => {
    //       console.log(error);
    //       // this.router.navigateByUrl("/clients/profile/"+this.client.id);
    //     }
    //   );
    // }
  }

  resendCompteInfo(numeroCompte: string) {
    this.comptesService.resendInfo(numeroCompte).subscribe(response => {
      console.log(response);
    },error => {
      console.log(error);
    })
  }
}
