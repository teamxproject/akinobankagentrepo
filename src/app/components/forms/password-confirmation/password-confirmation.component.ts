import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ClientsService } from '../../../services/clientsServices/clients.service';
import { User } from '../../../models/user';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ChangeClientDataRequest } from '../../../models/changeClientDataRequest';
import { ProfileService } from '../../../services/profileServices/profile.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Client } from 'src/app/models/client';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-password-confirmation',
  templateUrl: './password-confirmation.component.html',
  styleUrls: ['./password-confirmation.component.css'],
})
export class PasswordConfirmationComponent implements OnInit {
  hideCompteForm: boolean = false;
  hideAjouterButton: boolean = false;
  clientData = new ChangeClientDataRequest();
  errorMessage: any;
  messageRequest: any;
  message: string;
  sendPasswordFormGroup = new FormGroup({
    password: new FormControl('', Validators.required),
  });

  @Select((store) => store.state.selectedClient)
  client: Observable<User>;

  title: string;

  @Select((store) => store.state.profile)
  agent: Observable<User>;

  form: number;
  clientId: number;

  constructor(
    private _snackBar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private clientsService: ClientsService,
    private profileService: ProfileService,
    public dialogRef: MatDialogRef<PasswordConfirmationComponent>
  ) {
    console.log(data);
    // this.title = data.title;
    // this.client = data.client;
    // this.form = data.form;
    this.clientId = data.clientId;
    console.log(this.client);
  }

  ngOnInit(): void {
    this.getAgent();
    // this.getClient();
  }
  openSnackBar(message, action) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }

  // getClient(){
  //   this.clientsService.getClientProfile(this.clientId).subscribe(clientProfile => {
  //     this.client =<User> clientProfile;
  //   },error => {
  //     console.log(error);
  //   })
  // }
  click: boolean = false;
  done: boolean = false;

  getAgent() {
    // this.profileService.getAgentProfile().subscribe(
    //   (profile) => {
    //     this.agent = <User>profile;
    //   },
    //   (error) => {
    //     console.error(error);
    //   }
    // );
  }

  onSubmit() {
    this.click = true;
    this.profileService
      .checkPassword(this.sendPasswordFormGroup.value)
      .subscribe(
        () =>
          this.dialogRef.close(
            this.sendPasswordFormGroup.get('password').value
          ),
        (error) => alert(error.error.message)
      );
  }

  sendClientPasswordCompte() {
    // this.click = true;
    // console.log(this.sendPasswordFormGroup.value.agentPassword);
    // console.log(this.client);
    // this.clientData.agentPassword = this.sendPasswordFormGroup.value.agentPassword;
    // this.clientData.user = this.client;
    // this.clientsService
    //   .sendClientVerification(this.client.id, this.client)
    //   .subscribe(
    //     (response) => {
    //       this.messageRequest = response;
    //       this.message = this.messageRequest.text;
    //       this.errorMessage = null;
    //       this.done = true;
    //       this.click = false;
    //       this.openSnackBar('La modification est enregistrée', 'undo');
    //     },
    //     (error) => {
    //       console.log(error);
    //       this.errorMessage = error.error.message;
    //       this.done = false;
    //       this.click = false;
    //       // this.formClientPassword = true;
    //     }
    //   );
  }
  sendClientNewData() {
    // this.click = true;
    // console.log(this.sendPasswordFormGroup.value.agentPassword);
    // console.log(this.client);
    // this.clientData.agentPassword = this.sendPasswordFormGroup.value.agentPassword;
    // const { email, nom, prenom, numeroTelephone } = this.client;
    // this.clientData.user = { email, nom, prenom, numeroTelephone };
    // this.clientsService
    //   .sendClientDataVerification(this.client.id, this.clientData)
    //   .subscribe(
    //     (response) => {
    //       this.message = 'Enregistré avec succès.';
    //       this.errorMessage = null;
    //       this.done = true;
    //       this.click = false;
    //       this.openSnackBar('La modification est enregistrée', 'undo');
    //     },
    //     (error) => {
    //       console.log(error);
    //       this.done = false;
    //       this.click = false;
    //       this.errorMessage = error.error.message;
    //     }
    //   );
  }

  agentVerification() {
    console.log(this.form);
    if (this.form == 1) {
      return this.sendClientNewData();
    } else {
      return this.sendClientPasswordCompte();
    }
  }
}
