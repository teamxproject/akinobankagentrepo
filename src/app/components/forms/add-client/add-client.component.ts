import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from "@angular/material/dialog";
import {User} from "../../../models/user";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {ClientsService} from "../../../services/clientsServices/clients.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import {Store} from '@ngxs/store';
import {AddClient} from '../../../actions/clients.actions';

@Component({
  selector: 'app-add-client',
  templateUrl: './add-client.component.html',
  styleUrls: ['./add-client.component.css']
})
export class AddClientComponent implements OnInit {
  title:string;
  client : User;
  done:boolean=false;
  click:boolean=false;

  constructor(private store: Store,  private _snackBar:MatSnackBar, @Inject(MAT_DIALOG_DATA) public data:any , private clientsService:ClientsService) {
    this.title = data.title;
  }
  snackBarMessage :string = "Le client est ajouté avec succès."
  snackBarAction :string="undo";

  addClientFormGroup = new FormGroup({
    nom : new FormControl('',Validators.required),
    prenom : new FormControl('',Validators.required),
    email : new FormControl('',Validators.required),
    numeroTelephone : new FormControl('',Validators.required),
  })


  ngOnInit(): void {
  }

  openSnackBar() {
    this._snackBar.open(this.snackBarMessage, this.snackBarAction,  {
      duration: 2000,
    });
  }
  addClient(){
    this.click = true;

    this.client = this.addClientFormGroup.value;

    this.store.dispatch(new AddClient(this.addClientFormGroup.value))
      .subscribe(() => {
        this.click =false;
        this.openSnackBar();
        this.done= true;
      }, error => {
        console.log(error);
        this.click =false;
        this.done= false;

      });
    //console.log(this.client);
    // this.client.prenom = this.addClientFormGroup.value.prenom;
    // this.client.email = this.addClientFormGroup.value.email;
    // this.client.numeroTelephone = this.addClientFormGroup.value.numeroTelephone;
    // this.clientsService.addClient(this.client).subscribe(result => {
    //   console.log(result);
    //   this.click =false;
    //   this.openSnackBar();
    //   this.done= true;
    // },error => {
    //   console.log(error);
    //   this.click =false;
    //   this.done= false;
    //
    //
    // })

  }

}
