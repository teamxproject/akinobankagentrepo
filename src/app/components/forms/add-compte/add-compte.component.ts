import { Component, Inject, OnInit } from '@angular/core';
import {
  MAT_DIALOG_DATA,
  MatDialog,
  MatDialogConfig,
} from '@angular/material/dialog';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Client } from '../../../models/client';
import { User } from '../../../models/user';
import { Compte } from '../../../models/compte';
import { AddCompteVerification } from '../../../models/addCompteVerification';
import { ProfileService } from '../../../services/profileServices/profile.service';
import { ComptesService } from '../../../services/comptesService/comptes.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Store, Select } from '@ngxs/store';
import { AddCompte } from 'src/app/actions/clients.actions';
import { AppState } from 'src/app/states/app.state';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-add-compte',
  templateUrl: './add-compte.component.html',
  styleUrls: ['./add-compte.component.css'],
})
export class AddCompteComponent implements OnInit {
  client = this.store.selectSnapshot(AppState.selectOneClient);

  compte: AddCompteVerification;
  hideCompteForm: boolean = false;
  hideAjouterButton: boolean = false;
  errorMessage: string;
  snackBarmessage = 'Le compte est ajouté avec succès.';
  snackBarAction = 'undo';

  addCompteFormGroup = new FormGroup({
    intitule: new FormControl('', Validators.required),
    solde: new FormControl('', Validators.required),
    agentPassword: new FormControl('', Validators.required),
  });

  constructor(
    private store: Store,
    private _snackBar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private comptesService: ComptesService,
    private dialog: MatDialog
  ) {
    // this.client = data.client;
    console.log(data);
  }

  ngOnInit(): void {}

  // openSnackBar() {
  //   this._snackBar.open(this.snackBarmessage, this.snackBarAction, {
  //     duration: 2000,
  //   });
  // }
  done: boolean = false;
  click: boolean = false;

  openSnackBar() {
    this._snackBar.open(this.snackBarmessage, this.snackBarAction, {
      duration: 2000,
    });
  }

  addClientCompte() {
    this.click = true;
    this.compte = this.addCompteFormGroup.value;
    // this.client.subs
    this.store
      .dispatch(new AddCompte(this.client.id, this.addCompteFormGroup.value))
      .subscribe(
        () => {
          this.click = false;
          this.errorMessage = null;
          this.hideAjouterButton = true;
          this.openSnackBar();
          this.done = true;
        },
        (error) => {
          console.log(error);
          this.errorMessage = error.error.message;
          this.done = false;
          this.click = false;
        }
      );
    // this.comptesService.addCompte(this.client.id, this.compte).subscribe(
    //   (res) => {
    //     console.log(res);
    //     this.click = false;
    //     this.errorMessage = null;
    //     this.hideAjouterButton = true;
    //     this.openSnackBar();
    //     this.done = true;

    //     // window.location.reload();
    //   },
    //   (error) => {
    //     console.log(error);
    //     this.errorMessage = error.error.message;
    //     this.done = false;
    //     this.click = false;
    //   }
    // );
  }

  validerCompte() {
    this.hideCompteForm = true;
  }

  backToForm() {
    this.hideCompteForm = false;
  }
}
