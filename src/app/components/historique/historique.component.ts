import { Component, OnInit } from '@angular/core';
import { LogsService } from '../../services/logs/logs.service';
import { EventLogs } from '../../models/eventLogs';
import { User } from '../../models/user';
import { Client } from '../../models/client';
import { Store, Select } from '@ngxs/store';
import {
  GetClientsActivities,
  GetAgentActivities,
  AddActivitiy,
} from 'src/app/actions/activities.actions';
import { Observable } from 'rxjs';
import { Activity } from 'src/app/models/activity';
import { AppState } from 'src/app/states/app.state';
import { WebsocketService } from 'src/app/services/websocket.service';
import { TokenService } from 'src/app/services/token.service';
import { CookieService } from 'src/app/services/cookie.service';

@Component({
  selector: 'app-historique',
  templateUrl: './historique.component.html',
  styleUrls: ['./historique.component.css'],
})
export class HistoriqueComponent implements OnInit {
  // logs: EventLogs;
  agent: User;
  pages = [2];

  @Select(AppState.selectAgentActivities)
  activities: Observable<Activity[]>;
  constructor(
    private logsService: LogsService,
    private store: Store,
    private websocketService: WebsocketService,
    private tokenService: TokenService,
    private cookieService: CookieService
  ) {}

  ngOnInit(): void {
    // this.getEvents(0);
    this.store.dispatch(new GetAgentActivities());
    // let stompClient = this.websocketService.connect();
    // stompClient.connect(
    //   {
    //     Authorization: 'Bearer ' + this.tokenService.token,
    //     'X-XSRF-TOKEN': this.cookieService.get('XSRF-TOKEN'),
    //   },
    //   () => {
    //     stompClient.subscribe('/user/topic/activities', (data) => {
    //       const activity: Activity = JSON.parse(data.body);
    //       this.store.dispatch(new AddActivitiy(activity));
    //       console.log('Received : ', activity);
    //     });
    //   }
    // );
  }

  // getEvents(page) {
  //   this.logsService.getAllEvenets(page, 10).subscribe(
  //     (logs) => {
  //       this.logs = <EventLogs>logs;
  //       console.log(this.logs);
  //       console.log(this.logs.totalPages);
  //       this.pages = [];
  //       for (let i = 0; i < this.logs.totalPages; i++) {
  //         this.pages.push(i);
  //       }
  //       console.log(this.pages);
  //     },
  //     (error) => {
  //       console.log(error);
  //     }
  //   );
  // }
}
