import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {CompteRecoveryRequest} from "../../models/compteRecoveryRequest";
import {ForgetPasswordService} from "../../services/forgetPassword/forget-password.service";

@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password.component.html',
  styleUrls: ['./forget-password.component.css']
})
export class ForgetPasswordComponent implements OnInit {


  forgetFormGroup = new FormGroup({
    email: new FormControl('', Validators.required),
    operation: new FormControl('', Validators.required),
  });
  message:string;
  done:string;

  compteRequest : CompteRecoveryRequest;
  constructor(private forgetPasswordService:ForgetPasswordService) { }


  ngOnInit(): void {
  }

  forgetPassword() {
    this.compteRequest = this.forgetFormGroup.value;
    console.log(this.compteRequest);

    this.forgetPasswordService.forgetPassword(this.compteRequest).subscribe(res =>{
      console.log(res);
      this.done ="L'email a été envoyé avec succès";

    },error => {
      console.log(error)
      this.message = error.error.message;
    })

  }
}
