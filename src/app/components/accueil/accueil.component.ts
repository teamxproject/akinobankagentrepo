import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { ProfileService } from '../../services/profileServices/profile.service';
import { User } from '../../models/user';
import { ClientsService } from '../../services/clientsServices/clients.service';
import { Client } from '../../models/client';
import { Select, Store } from '@ngxs/store';
import { GetProfile } from '../../actions/profile.actions';
import { GetClients } from '../../actions/clients.actions';
import { AppState } from '../../states/app.state';
import { Observable, Subscription } from 'rxjs';
import { MainStore } from '../../store';
import { WebsocketService } from 'src/app/services/websocket.service';
import { TokenService } from 'src/app/services/token.service';
import { CookieService } from 'src/app/services/cookie.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Notification } from '../../models/notification';
import {
  AddNotification,
  FetchNotifications,
} from 'src/app/actions/notifications.actions';
@Component({
  selector: 'app-accueil',
  templateUrl: './accueil.component.html',
  styleUrls: ['./accueil.component.css'],
})
export class AccueilComponent implements OnInit, OnDestroy {
  @Select((store) => store.state.profile)
  agent: Observable<User>;

  @Select((store) => store.state.clients)
  clients: Observable<Client[]>;

  websocketSusbscription: Subscription;

  notificationsCount = 0;

  constructor(
    private store: Store,
    private clientsService: ClientsService,
    private profileService: ProfileService,
    private websocketService: WebsocketService,
    private tokenService: TokenService,
    private cookieService: CookieService,
    private router: Router,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.store.dispatch(new FetchNotifications());

    this.store.dispatch(new GetClients());
    this.websocketService.connect();
    this.websocketSusbscription = this.websocketService
      .getStomp()
      .subscribe('/user/topic/notifications')
      .subscribe((data) => {
        const notification: Notification = JSON.parse(data.body);
        this.openSnackBar(notification.contenu);
        this.notificationsCount++;
        this.store.dispatch(new AddNotification(notification));

        console.log('Received : ', JSON.parse(data.body));
      });
  }

  ngOnDestroy() {
    this.websocketSusbscription.unsubscribe();
  }

  goClients() {
    this.router.navigateByUrl('/clients');
  }
  goDemandes() {
    this.router.navigateByUrl('/demandes');
  }

  openSnackBar(message: string) {
    this.snackBar.open(message, 'OK', {
      duration: 10000,
    });
  }
}
