import { Component, OnInit } from '@angular/core';
import { ProfileService } from '../../services/profileServices/profile.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Client } from '../../models/client';
import { Agent } from '../../models/agent';
import { User } from '../../models/user';
import { Password } from '../../models/password';
import { Agence } from '../../models/agence';
import { Router } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Select, Store } from '@ngxs/store';
import {
  GetProfile,
  UnsetPhoto,
  UpdateProfile,
  UpdatePhoto,
} from '../../actions/profile.actions';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
})
export class ProfileComponent implements OnInit {
  agentSettingsFromGroup: FormGroup;

  @Select((store) => store.state.profile)
  profile: Observable<User>;
  password: Password;
  modifyAgent: boolean = false;
  AgentContact: boolean = false;
  passwordWrongError: boolean = false;
  passwordWrongmessage: string;
  passwordMatchError: boolean = false;
  saved: boolean = false;

  passwordFromGroup = new FormGroup({
    oldPassword: new FormControl('', Validators.required),
    newPassword: new FormControl('', [
      Validators.required,
      Validators.minLength(8),
    ]),
    confPassword: new FormControl('', [
      Validators.required,
      Validators.minLength(8),
    ]),
  });
  profileImage: any;
  selectedImage: File;
  errorMessageImage: boolean = false;
  buttonDeleteImage: boolean = false;
  agentContactFromGroup: FormGroup;
  hide = true;

  constructor(
    private _snackBar: MatSnackBar,
    private profileService: ProfileService,
    private router: Router,
    private sanitizer: DomSanitizer,
    private store: Store
  ) {}

  ngOnInit(): void {
    this.agentSettingsFromGroup = new FormGroup({
      nom: new FormControl('', Validators.required),
      prenom: new FormControl('', Validators.required),
      email: new FormControl('', [Validators.required, Validators.email]),
      numeroTelephone: new FormControl('', Validators.required),
    });

    this.agentContactFromGroup = new FormGroup({
      ville: new FormControl('', Validators.required),
      adresse: new FormControl('', Validators.required),
    });
    // this.store.dispatch(new GetProfile());
    this.profile.subscribe((data) => {
      if (data) {
        this.agentContactFromGroup.patchValue(data);
        this.agentSettingsFromGroup.patchValue(data);
      }
    });
  }

  openSnackBar(snackBarmessage, snackBarAction) {
    this._snackBar.open(snackBarmessage, snackBarAction, {
      duration: 2000,
    });
  }

  refresh() {
    this.router
      .navigateByUrl('/profile', { skipLocationChange: true })
      .then(() => {
        this.router.navigate([ProfileComponent]);
      });
  }

  getProfile() {
    // this.profileService.getAgentProfile().subscribe(
    //   (profile) => {
    //     console.log(profile);
    //
    //     //  this.profile = <User>profile;
    //
    //     if (this.profile.photo != null) {
    //       this.onDownloadImage();
    //     }
    //
    //
    //   },
    //   (error) => {
    //     console.log(error);
    //   }
    // );
  }

  selectPhoto($event: Event) {
    // @ts-ignore
    this.selectedImage = $event.target.files[0];
    console.log(this.selectedImage);
  }

  onUploadImage() {
    this.profileService.uploadImage(this.selectedImage).subscribe(
      (response: any) => {
        console.log(response);
        this.store.dispatch(new UpdatePhoto(response.name));
        this.openSnackBar('La photo est chargée avec succès', 'OK');

        // this.getProfile();
        // window.location.reload();
      },
      (error) => {
        console.log(error);
        this.errorMessageImage = error.error.message;
      }
    );
  }

  onDownloadImage() {
    // this.profileService.downloadImage(this.profile.photo).subscribe(
    //   (response) => {
    //     console.log(response);
    //     let objectURL = URL.createObjectURL(response);
    //     this.profileImage = this.sanitizer.bypassSecurityTrustUrl(objectURL);
    //     this.buttonDeleteImage = true;
    //     // this.createImageFromBlob(response);
    //     // this.refresh();
    //     // this.buttonDeleteImage=true;
    //   },
    //   (error) => {
    //     console.log(error);
    //   }
    // );
  }

  // onDeleteImage() {
  // let conf = confirm('Êtes-vous sûr ?');
  // if (conf) {
  //   this.profileService.deleteImage(this.profile.photo).subscribe(
  //     (response) => {
  //       console.log(response);
  //       this.buttonDeleteImage = false;
  //       // window.location.reload();
  //       this.openSnackBar('Votre image est supprimée avec succès', 'undo');
  //       this.getProfile();
  //     },
  //     (error) => {
  //       this.buttonDeleteImage = true;
  //       console.log(error);
  //     }
  //   );
  // }
  // }

  // createImageFromBlob(image: Blob) {
  //   let reader = new FileReader();
  //   reader.addEventListener("load", () => {
  //     this.profileImage = reader.result;
  //     this.buttonDeleteImage=true;
  //
  //   }, false);
  //   if (image) {
  //     reader.readAsDataURL(image);
  //   }
  // }
  onDeleteImage() {
    let conf = confirm('Êtes-vous sûr ?');
    if (conf) {
      this.store.dispatch(new UnsetPhoto());
      // this.profileService.deleteImage().subscribe(response =>{
      //   console.log(response);
      //   this.refresh();
      //
      // },error => {
      //   console.log(error);
      // })
    }
  }

  checkPasswords() {
    return this.passwordFromGroup.value.newPassword ==
      this.passwordFromGroup.value.confPassword
      ? this.sendNewPassword()
      : (this.passwordMatchError = true);
  }

  sendNewPassword() {
    this.passwordMatchError = false;
    console.log(this.passwordFromGroup.value);
    this.password = this.passwordFromGroup.value;
    console.log(this.password);

    this.profileService.changePassword(this.password).subscribe(
      (response) => {
        console.log(response);
        this.passwordWrongError = false;
        this.saved = true;
        this.openSnackBar('Enregistré avec succès', 'undo');
      },
      (error) => {
        this.saved = false;
        this.passwordWrongmessage = error.error.message;
        this.passwordWrongError = true;
        this.passwordMatchError = false;
      }
    );
  }

  modifyAgentInfo() {
    this.store
      .dispatch(new UpdateProfile(this.agentSettingsFromGroup.value))
      .subscribe(
        () => {
          this.modifyAgent = true;
          this.openSnackBar('Enregistré avec succès', 'undo');
        },
        (error) => {
          this.modifyAgent = false;
        }
      );
    // this.profile.nom = this.agentSettingsFromGroup.value.nom;
    // this.profile.prenom = this.agentSettingsFromGroup.value.prenom;
    // this.profile.email = this.agentSettingsFromGroup.value.email;
    // this.profile.numeroTelephone = this.agentSettingsFromGroup.value.numeroTelephone;
    // this.profileService
    //   .modifyProfile(this.agentSettingsFromGroup.value)
    //   .subscribe(
    //     (response) => {
    //       console.log(response);
    //       this.modifyAgent = true;
    //       this.openSnackBar('Enregistré avec succès', 'undo');
    //     },
    //     (error) => {
    //       console.log(error);
    //       this.modifyAgent = false;
    //     }
    //   );
  }

  modifyAgentContact() {
    this.store
      .dispatch(new UpdateProfile(this.agentContactFromGroup.value))
      .subscribe(
        () => {
          this.AgentContact = true;
          this.openSnackBar('Enregistré avec succès', 'undo');
        },
        (error) => {
          this.AgentContact = false;
        }
      );
    // console.log(this.agentContactFromGroup.value);
    // this.profile.ville = this.agentContactFromGroup.value.ville;
    // this.profile.adresse = this.agentContactFromGroup.value.adresse;
    // this.profileService.modifyProfile(this.profile).subscribe(
    //   (response) => {
    //     console.log(response);
    //     this.AgentContact = true;
    //     this.openSnackBar('Enregistré avec succès', 'undo');
    //   },
    //   (error) => {
    //     console.log(error);
    //     this.AgentContact = false;
    //   }
    // );
  }
}
