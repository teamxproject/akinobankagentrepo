import { Component, OnInit, OnDestroy } from '@angular/core';
import { LogsService } from '../../services/logs/logs.service';
import { EventLogs } from '../../models/eventLogs';
import { User } from '../../models/user';
import { Client } from '../../models/client';
import { Store, Select } from '@ngxs/store';
import {
  GetClientsActivities,
  AddActivitiy,
} from 'src/app/actions/activities.actions';
import { Observable, Subscription } from 'rxjs';
import { Activity } from 'src/app/models/activity';
import { AppState } from 'src/app/states/app.state';
import { WebsocketService } from 'src/app/services/websocket.service';
import { TokenService } from 'src/app/services/token.service';
import { CookieService } from 'src/app/services/cookie.service';

@Component({
  selector: 'app-logs',
  templateUrl: './logs.component.html',
  styleUrls: ['./logs.component.css'],
})
export class LogsComponent implements OnInit, OnDestroy {
  // logs: EventLogs;
  agent: User;
  pages = [2];

  @Select(AppState.selectClientsActivities)
  activities: Observable<Activity[]>;

  websocketSusbscription: Subscription;

  constructor(
    private logsService: LogsService,
    private store: Store,
    private websocketService: WebsocketService,
    private tokenService: TokenService,
    private cookieService: CookieService
  ) {}

  ngOnInit(): void {
    console.log("OPEN LOGS")
    // this.getEvents(0);
    this.store.dispatch(new GetClientsActivities());
    this.activities.subscribe(activities => {
      console.log(activities);

    })

    // let stompClient = this.websocketService.connect();
    this.websocketSusbscription = this.websocketService
      .getStomp()
      .subscribe('/user/topic/activities.clients')
      .subscribe((data) => {
        const activity: Activity = JSON.parse(data.body);
        this.store.dispatch(new AddActivitiy(activity));
        console.log('Received : ', activity);
      });
  }

  ngOnDestroy() {
    this.websocketSusbscription.unsubscribe();
  }

  // getEvents(page) {
  //   this.logsService.getAllEvenets(page, 10).subscribe(
  //     (logs) => {
  //       this.logs = <EventLogs>logs;
  //       console.log(this.logs);
  //       console.log(this.logs.totalPages);
  //       this.pages = [];
  //       for (let i = 0; i < this.logs.totalPages; i++) {
  //         this.pages.push(i);
  //       }
  //       console.log(this.pages);
  //     },
  //     (error) => {
  //       console.log(error);
  //     }
  //   );
  // }
}
