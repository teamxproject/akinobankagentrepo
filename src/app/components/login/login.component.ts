import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { LoginService } from '../../services/login/login.service';
import { User } from '../../models/user';
import { Token } from '../../models/token';
import * as jwt_decode from 'jwt-decode';
import { TokenService } from '../../services/token.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  stat: boolean;
  agent?: User;
  message: string;
  token: Token;

  loginFormGroup = new FormGroup({
    email: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required),
  });

  constructor(
    private router: Router,
    private loginService: LoginService,
    private tokenService: TokenService
  ) {}

  ngOnInit(): void {
    this.tokenService
      .getXSRFToken()
      .subscribe(() => console.log('XSRF loaded'));
  }

  onLoginIn() {
    console.log(this.loginFormGroup.value);
    this.agent = this.loginFormGroup.value;
    this.loginService.loginIn(this.agent).subscribe(
      (response: any) => {
        console.log(response);
        this.token = <Token>response;
        this.tokenService.setToken(response.token);

        var decoded = jwt_decode(this.token.token);
        console.log(decoded.user);
        this.router.navigateByUrl('/accueil');
      },
      (error) => {
        console.log(error);
        this.message = error.error.message;
      }
    );
  }
}
