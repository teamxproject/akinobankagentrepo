import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from "@angular/material/dialog";
import {Client} from "../../models/client";
import {CompteStatus} from "../../models/enum/compte-status";
import {Compte} from "../../models/compte";
import {SuppClient} from "../../actions/clients.actions";
import {Store} from "@ngxs/store";
import {AcceptDemande, RefuseDemande} from "../../actions/demandes.actions";

@Component({
  selector: 'app-demandes',
  templateUrl: './demandes.component.html',
  styleUrls: ['./demandes.component.css']
})
export class DemandesComponent implements OnInit {


  client:Client;
  comptes:Compte[];
  constructor(@Inject(MAT_DIALOG_DATA) public data:any , private store:Store) {
    this.client = data.client
    this.comptes = data.client.comptes;
    console.log(this.client)

  }

  ngOnInit(): void {
  }

  acceptDemande(numeroCompte) {
    this.store.dispatch(new AcceptDemande(numeroCompte));
  }



  deleteDemande(numeroCompte) {
    this.store.dispatch(new RefuseDemande(numeroCompte));

  }
}
