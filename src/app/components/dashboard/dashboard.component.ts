import { Component, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { User } from 'src/app/models/user';
import { Client } from 'src/app/models/client';
import { ClientsService } from 'src/app/services/clientsServices/clients.service';
import { ProfileService } from 'src/app/services/profileServices/profile.service';
import { Router } from '@angular/router';
import { GetProfile } from 'src/app/actions/profile.actions';
import { GetClients } from 'src/app/actions/clients.actions';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
})
export class DashboardComponent implements OnInit {
  @Select((store) => store.state.profile)
  agent: Observable<User>;

  @Select((store) => store.state.clients)
  clients: Observable<Client[]>;

  constructor(
    private store: Store,
    private clientsService: ClientsService,
    private profileService: ProfileService,
    private router: Router
  ) {}

  ngOnInit(): void {
    // this.store.dispatch(new GetProfile());
    this.store.dispatch(new GetClients());
  }

  goClients() {
    this.router.navigateByUrl('/clients');
  }
  goDemandes() {
    this.router.navigateByUrl('/demandes');
  }
}
