import { Component, OnInit } from '@angular/core';
import { ProfileService } from '../../services/profileServices/profile.service';
import { User } from '../../models/user';
import { Notification } from '../../models/notification';
import { Router } from '@angular/router';
import { TokenService } from 'src/app/services/token.service';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { GetProfile } from '../../actions/profile.actions';
import { MarkAllAsSeen } from 'src/app/actions/notifications.actions';

@Component({
  selector: 'app-navbar-header',
  templateUrl: './navbar-header.component.html',
  styleUrls: ['./navbar-header.component.css'],
})
export class NavbarHeaderComponent implements OnInit {
  @Select((store) => store.state.profile)
  profile: Observable<User>;

  notificationsCount = 0;

  @Select((store) => store.state.notifications)
  notifications: Observable<Notification>;

  notificationsOpen = false;

  profileImage: any;
  constructor(
    private tokenService: TokenService,
    private profileService: ProfileService,
    private router: Router,
    private store: Store
  ) {}

  ngOnInit(): void {
    this.store
      .select((store) => store.state.notifications)
      .subscribe((notifications) => {
        console.log('Received notif', notifications);
        if (notifications)
          notifications.forEach((element) => {
            if (!element.lue) this.notificationsCount++;
          });
      });
    // this.store.dispatch(new GetProfile());
  }

  showNotifications() {
    this.notificationsOpen = true;
  }

  closeNotifications() {
    this.notificationsOpen = false;
    if (this.notificationsCount > 0) {
      this.store.dispatch(new MarkAllAsSeen());
      this.notificationsCount = 0;
    }
  }

  // //getProfile() {
  //   this.profileService.getAgentProfile().subscribe(
  //     (response) => {
  //       console.log(response);
  //       this.profile = <User>response;
  //       if (this.profile?.photo != null) {
  //         this.onDownloadImage();
  //         console.log('nav bar ');
  //
  //         console.log(response);
  //       }
  //     },
  //     (error) => {
  //       console.log(error);
  //       // this.profileService.test(profile.refresh_token);
  //     }
  //   );
  // }
  onDownloadImage() {
    // this.profileService.downloadImage(this.profile?.photo).subscribe(
    //   (image) => {
    //     this.createImageFromBlob(image);
    //   },
    //   (error) => {
    //     console.log(error);
    //     // this.profileService.test();
    //   }
    // );
  }

  createImageFromBlob(image: Blob) {
    let reader = new FileReader();
    reader.addEventListener(
      'load',
      () => {
        this.profileImage = reader.result;
      },
      false
    );
    if (image) {
      reader.readAsDataURL(image);
    }
  }

  logout() {
    this.tokenService.unsetToken();
    this.router.navigateByUrl('/login');
  }
}
