import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AccueilComponent } from './components/accueil/accueil.component';
import { ClientsComponent } from './components/clients/clients.component';
import { ProfileComponent } from './components/profile/profile.component';
import { ComptesComponent } from './components/comptes/comptes.component';
import { ClientProfileComponent } from './components/clients/client-profile/client-profile.component';
import { LogsComponent } from './components/logs/logs.component';
import { LoginComponent } from './components/login/login.component';
import { CompteProfileComponent } from './components/clients/compte-profile/compte-profile.component';
import { Error404Component } from './components/error404/error404.component';
import { AuthenticatedGuard } from './guards/authenticated.guard';
import { GuestGuard } from './guards/guest.guard';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ProfileResolver } from './resolvers/profile.resolver';
import { ClientProfileResolver } from './resolvers/client-profile.resolver';
import { CompteDetailsResolver } from './resolvers/compte-details.resolver';
import { HistoriqueComponent } from './components/historique/historique.component';
import {ForgetPasswordComponent} from "./components/forget-password/forget-password.component";

const routes: Routes = [
  {
    path: 'accueil',
    component: AccueilComponent,
    canActivate: [AuthenticatedGuard],
    resolve: { profile: ProfileResolver },
    children: [
      {
        path: '',
        component: DashboardComponent,
        canActivate: [AuthenticatedGuard],
      },
      {
        path: 'clients',
        component: ClientsComponent,
        canActivate: [AuthenticatedGuard],
      },
      {
        path: 'comptes',
        component: ComptesComponent,
        canActivate: [AuthenticatedGuard],
      },
      {
        path: 'clients/profile/:id',
        component: ClientProfileComponent,
        canActivate: [AuthenticatedGuard],
        resolve: { client: ClientProfileResolver },
      },
      {
        path: 'profile',
        component: ProfileComponent,
        canActivate: [AuthenticatedGuard],
      },
      {
        path: 'clients/profile/:id/compte/:id',
        component: CompteProfileComponent,
        canActivate: [AuthenticatedGuard],
        resolve: { compte: CompteDetailsResolver },
      },

      {
        path: 'logs',
        component: LogsComponent,
        canActivate: [AuthenticatedGuard],
      },
      {
        path: 'historique',
        component: HistoriqueComponent,
        canActivate: [AuthenticatedGuard],
      },
    ],
  },
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full',
  },
  {
    path: 'forget-password',
    component: ForgetPasswordComponent,
  },
  {
    path: 'error404',
    component: Error404Component,
  },
  {
    path: 'login',
    component: LoginComponent,
    canActivate: [GuestGuard],
  },
  {
    path: '**',
    component: Error404Component,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
