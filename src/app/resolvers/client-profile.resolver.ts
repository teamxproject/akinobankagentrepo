import { map, tap, catchError } from 'rxjs/operators';
import {
  Resolve,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router,
} from '@angular/router';
import { Observable, of } from 'rxjs';
import { Injectable } from '@angular/core';
import { Store } from '@ngxs/store';
import { User } from '../models/user';
import { SetSelectedClient } from '../actions/clients.actions';
import { AppState } from '../states/app.state';
import { Error404Component } from '../components/error404/error404.component';

@Injectable({
  providedIn: 'root',
})
export class ClientProfileResolver implements Resolve<User> {
  constructor(private store: Store, private router: Router) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<User> {
    return this.store
      .dispatch(new SetSelectedClient(parseInt(route.paramMap.get('id'), 10)))
      .pipe(
        map(() => this.store.selectSnapshot(AppState)),
        catchError((error) => {
          if (error.status === 404) {
            console.log('NOT FOUND');
            this.router.navigate([Error404Component]);
          }
          return of(error);
        })
      );
  }
}
