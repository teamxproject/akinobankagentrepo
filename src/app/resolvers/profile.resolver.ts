import { map, tap } from 'rxjs/operators';
import {
  Resolve,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
} from '@angular/router';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { Store } from '@ngxs/store';
import { GetProfile } from '../actions/profile.actions';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root',
})
export class ProfileResolver implements Resolve<User> {
  constructor(private store: Store) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<User> {
    return this.store.dispatch(new GetProfile());
  }
}
