import { Pipe, PipeTransform } from '@angular/core';
import { Observable } from 'rxjs';
import { ClientsService } from './services/clientsServices/clients.service';
import { tap, map } from 'rxjs/operators';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { ProfileService } from './services/profileServices/profile.service';

@Pipe({
  name: 'photo',
})
export class PhotoPipe implements PipeTransform {
  constructor(
    private clientsService: ClientsService,
    private profileService: ProfileService,
    private sanitizer: DomSanitizer
  ) {}

  transform(photo: string, profil = false): Observable<SafeUrl> {
    return !profil
      ? this.clientsService
          .getClientImage(photo)
          .pipe(
            map((image) =>
              this.sanitizer.bypassSecurityTrustUrl(URL.createObjectURL(image))
            )
          )
      : this.profileService
          .downloadImage(photo)
          .pipe(
            map((image) =>
              this.sanitizer.bypassSecurityTrustUrl(URL.createObjectURL(image))
            )
          );
  }
}
