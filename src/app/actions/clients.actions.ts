import { Compte } from '../models/compte';
import { Client } from '../models/client';
import { AddCompteVerification } from '../models/addCompteVerification';
import { User } from '../models/user';

export class GetClients {
  static readonly type = '[Clients] GET';
}

export class SetSelectedClient {
  static readonly type = '[Clients] GET ONE';
  constructor(public id: number) {}
}

export class AddCompte {
  static readonly type = '[Comptes] CREATE';
  constructor(public id: number, public payload: AddCompteVerification) {}
}
export class AddClient {
  static readonly type = '[Clients] CREATE';
  constructor(public payload: User) {}
}

export class SetSelectedCompte {
  static readonly type = '[Comptes] GET One';
  constructor(public id: number) {}
}

export class ModCompte {
  static readonly type = '[Comptes] UPDATE';
  constructor(public id: number, public payload: Compte) {}
}
export class SuppCompte {
  static readonly type = '[Comptes] DELETE';
  constructor(public id: string) {}
}
export class ModClient {
  static readonly type = '[Clients] UPDATE';
  constructor(
    public id: number,
    public payload: { user: User; agentPassword: string }
  ) {}
}

export class ModClientContacts {
  static readonly type = '[Clients] UPDATE CONTACTS';
  constructor(
    public id: number,
    public payload: { user: User; agentPassword: string }
  ) {}
}
export class SuppClient {
  static readonly type = '[Clients] DELETE';
  constructor(public id: number) {}
}



