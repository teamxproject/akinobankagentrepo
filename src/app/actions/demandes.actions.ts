import {AddCompteVerification} from "../models/addCompteVerification";

export class AcceptDemande {
  static readonly type = '[Demande] Accepted';
  constructor(public numeroCompte: string) {}

}

export class RefuseDemande {
  static readonly type = '[Demande] Rejected';
  constructor(public numeroCompte: string) {}
}
