import { Activity } from '../models/activity';

export class GetClientsActivities {
  static readonly type = '[Activties] GET CLIENTS';
}

export class GetAgentActivities {
  static readonly type = '[Activties] GET AGENT';
}

export class GetSelectedClientActivities {
  static readonly type = '[Activties] GET SELECTED CLIENT';
}

export class AddActivitiy {
  static readonly type = '[Activites] Add';

  constructor(public payload: Activity) {}
}
