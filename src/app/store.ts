import { User } from './models/user';
import { Compte } from './models/compte';
import { Activity } from './models/activity';
import { Notification } from './models/notification';

export class MainStore {
  profile: User;
  clients: User[];
  selectedCompte: Compte;
  selectedClient: User;
  activities: {
    agent: Activity[];
    clients: Activity[];
    specific: Activity[];
  };
  notifications: Notification[];
  // comptes:
}
