import { Injectable } from '@angular/core';
import { State, StateContext, Action, Selector, Actions } from '@ngxs/store';
import { MainStore } from '../store';
import {
  AddCompte,
  GetClients,
  SetSelectedClient,
  ModCompte,
  SuppCompte,
  ModClient,
  SuppClient,
  SetSelectedCompte,
  AddClient,
  ModClientContacts,
} from '../actions/clients.actions';
import {
  GetProfile,
  UpdatePhoto,
  UnsetPhoto,
  UpdateProfile,
} from '../actions/profile.actions';
import {AcceptDemande , RefuseDemande} from "../actions/demandes.actions";
import { ComptesService } from '../services/comptesService/comptes.service';
import { tap, map } from 'rxjs/operators';
import {
  patch,
  updateItem,
  removeItem,
  insertItem,
} from '@ngxs/store/operators';
import { ClientsService } from '../services/clientsServices/clients.service';
import { User } from '../models/user';
import { DomSanitizer } from '@angular/platform-browser';
import { ProfileService } from '../services/profileServices/profile.service';
import { Compte } from '../models/compte';
import { A } from '@angular/cdk/keycodes';
import {
  GetClientsActivities,
  GetAgentActivities,
  GetSelectedClientActivities,
  AddActivitiy,
} from '../actions/activities.actions';
import { ActivitiesService } from '../services/activities.service';
import {
  FetchNotifications,
  AddNotification,
  MarkAllAsSeen,
} from '../actions/notifications.actions';
import { NotificationsService } from '../services/notifications.service';
import {DemandesService} from "../services/demandes/demandes.service";

@Injectable()
@State({
  name: 'state',
})
export class AppState {
  constructor(
    private clientService: ClientsService,
    private compteService: ComptesService,
    private profileService: ProfileService,
    private activitiesService: ActivitiesService,
    private sanitizer: DomSanitizer,
    private notificationService: NotificationsService,
    private demandedService:DemandesService
  ) {}

  @Selector()
  static selectClients(store: MainStore) {
    return store.clients;
  }

  @Selector()
  static selectClientsActivities(store: MainStore) {
    return store.activities.clients;
  }
  @Selector()
  static selectAgentActivities(store: MainStore) {
    return store.activities.agent;
  }

  @Selector()
  static selectSelectedClientActivities(store: MainStore) {
    return store.activities.specific;
  }

  @Selector()
  static selectOneClient(store: MainStore) {
    return store.selectedClient;
  }

  @Action(GetClients)
  fetchClients(ctx: StateContext<MainStore>) {
    return this.clientService
      .getAllClients()
      .pipe(tap((res) => ctx.patchState({ clients: res })));
  }

  @Action(SetSelectedClient)
  getClientById(ctx: StateContext<MainStore>, { id }: SetSelectedClient) {
    return this.clientService
      .getClientProfile(id)
      .pipe(tap((res) => ctx.patchState({ selectedClient: res })));
  }

  @Action(SetSelectedCompte)
  getCompteById(ctx: StateContext<MainStore>, { id }: SetSelectedCompte) {
    return this.compteService
      .getCompte(id)
      .pipe(tap((res) => ctx.patchState({ selectedCompte: res })));
  }

  @Action(AddCompte)
  createCompte(ctx: StateContext<MainStore>, { id, payload }: AddCompte) {
    return this.compteService.addCompte(id, payload).pipe(
      tap((res) => {
        console.log('Compte added for ' + id, res);
        //   let client = ctx.getState().clients.find((client) => client.id === id);
        //   client = { comptes: [...client.comptes, res] };
        //   ctx.setState(
        //     patch({ clients: updateItem((client) => client.id === id, client) })
        //   );
        const selectedClient = ctx.getState().selectedClient;
        const client: User = {
          ...selectedClient,
          comptes: [...selectedClient.comptes, res],
        };
        ctx.setState(patch({ selectedClient: client }));
      })
    );
  }
  @Action(GetProfile)
  fetchProfile(ctx: StateContext<MainStore>) {
    return this.profileService
      .getAgentProfile()
      .pipe(tap((res) => ctx.patchState({ profile: res })));
  }

  @Action(UpdatePhoto)
  updatePhoto(ctx: StateContext<MainStore>, { url }: UpdatePhoto) {
    return ctx.patchState({
      profile: { ...ctx.getState().profile, photo: url },
    });
  }

  @Action(AddClient)
  addClient(ctx: StateContext<MainStore>, { payload }: AddClient) {
    return this.clientService
      .addClient(payload)
      .pipe(tap((res) => ctx.setState(patch({ clients: insertItem(res) }))));
  }
  @Action(ModCompte)
  updateCompte(ctx: StateContext<MainStore>, { id, payload }: ModCompte) {
    return this.compteService
      .modifyCompte(id, payload)
      .pipe(tap((res) => ctx.patchState({ selectedCompte: res })));
  }

  @Action(ModClient)
  updateClient(ctx: StateContext<MainStore>, { id, payload }: ModClient) {
    return this.clientService
      .modifyClientSettings(id, payload)
      .pipe(tap((res) => ctx.patchState({ selectedClient: res })));
  }

  @Action(ModClientContacts)
  updateClientContact(
    ctx: StateContext<MainStore>,
    { id, payload }: ModClientContacts
  ) {
    return this.clientService
      .modifyClientContacts(id, payload)
      .pipe(tap((res) => ctx.patchState({ selectedClient: res })));
  }

  @Action(UnsetPhoto)
  unsetPhoto(ctx: StateContext<MainStore>) {
    return this.profileService.deleteImage().pipe(
      tap(() =>
        ctx.patchState({
          profile: { ...ctx.getState().profile, photo: null },
        })
      )
    );
  }

  @Action(SuppCompte)
  deleteCompte(ctx: StateContext<MainStore>, { id }: SuppCompte) {
    return this.compteService.deleteCompte(id).pipe(
      tap((res) => {
        const selectedClient = ctx.getState().selectedClient;
        const client: User = {
          ...selectedClient,
          comptes: selectedClient.comptes.filter(
            (compte) => compte.numeroCompte !== id
          ),
        };
        ctx.patchState({ selectedClient: client });
      })
    );
  }

  @Action(SuppClient)
  deleteClient(ctx: StateContext<MainStore>, { id }: SuppClient) {
    return this.clientService.deleteClient(id).pipe(
      tap((res) => {
        ctx.setState(
          patch({ clients: removeItem((client) => client.id === id) })
        );
      })
    );
  }

  @Action(UpdateProfile)
  updateProfile(ctx: StateContext<MainStore>, { payload }: UpdateProfile) {
    return this.profileService
      .updateProfile(payload)
      .pipe(tap((res) => ctx.patchState({ profile: res })));
  }

  @Action(GetClientsActivities)
  fetchClientsActivities(ctx: StateContext<MainStore>) {
    return this.activitiesService.fetchClientsActivities().pipe(
      tap((activities) =>
        ctx.patchState({
          activities: { ...ctx.getState().activities, clients: activities },
        })
      )
    );
  }

  @Action(AddActivitiy)
  addActivity(ctx: StateContext<MainStore>, { payload }: AddActivitiy) {
    return ctx.setState(
      patch({
        activities: {
          ...ctx.getState().activities,
          clients: [payload, ...ctx.getState().activities.clients],
        },
      })
    );
  }

  @Action(GetAgentActivities)
  fetchAgentActivities(ctx: StateContext<MainStore>) {
    return this.activitiesService.fetchAgentActivities().pipe(
      tap((activities) =>
        ctx.patchState({
          activities: { ...ctx.getState().activities, agent: activities },
        })
      )
    );
  }

  @Action(GetSelectedClientActivities)
  fetchSelectedClientActivities(ctx: StateContext<MainStore>) {
    return this.clientService
      .fetchClientActivities(ctx.getState().selectedClient.id)
      .pipe(
        tap((activities) =>
          ctx.patchState({
            activities: { ...ctx.getState().activities, specific: activities },
          })
        )
      );
  }

  @Action(FetchNotifications)
  fetchNotifications(ctx: StateContext<MainStore>) {
    return this.notificationService
      .get()
      .pipe(tap((notifications) => ctx.patchState({ notifications })));
  }

  @Action(AddNotification)
  addNotification(ctx: StateContext<MainStore>, { payload }: AddNotification) {
    return ctx.setState(patch({ notifications: insertItem(payload) }));
  }

  @Action(MarkAllAsSeen)
  markSeen(ctx: StateContext<MainStore>) {
    return this.notificationService.markAsSeen().pipe(
      tap(() =>
        ctx.patchState({
          notifications: ctx.getState()
              .notifications.map(
                (notification) => (notification = { ...notification, lue: true })
              ),
        })
      )
    );
  }

  @Action(AcceptDemande)
  acceptDemande(ctx: StateContext<MainStore>, { numeroCompte }: AcceptDemande) {
    return this.demandedService
      .acceptDemande(numeroCompte)
      .pipe(tap((res) => ctx.patchState({ selectedClient: res })));
  }

  @Action(RefuseDemande)
  refuseDemande(ctx: StateContext<MainStore>, { numeroCompte }: RefuseDemande) {
    return this.demandedService
      .rejectDemende(numeroCompte)
      .pipe(tap((res) => ctx.patchState({ selectedClient: res })));
  }
}
