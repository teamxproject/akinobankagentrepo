import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { Injectable } from '@angular/core';
import { TokenService } from './services/token.service';
import { catchError, switchMap } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable()
export class RequestsInterceptor implements HttpInterceptor {
  constructor(private tokenService: TokenService, private router: Router) {}

  private refreshTokenSubject: BehaviorSubject<any> = new BehaviorSubject<any>(
    null
  );

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const token = localStorage.getItem('token');
    token !== undefined && token !== null
      ? (req = this.addToken(req, localStorage.getItem('token')))
      : (req = this.addToken(req, ''));
    return next.handle(req).pipe(
      catchError((error) => {
        if (error instanceof HttpErrorResponse && error.status === 401) {
          return this.handle401Error(req, next);
        } else {
          return throwError(error);
        }
      })
    );
  }

  private addToken(request: HttpRequest<any>, token: string) {
    if (request.method === 'GET')
      return request.clone({
        setHeaders: { Authorization: `Bearer ${token}` },
      });
    return request.clone({
      setHeaders: { Authorization: `Bearer ${token}` },
      withCredentials: true,
    });
  }

  private handle401Error(request: HttpRequest<any>, next: HttpHandler) {
    this.refreshTokenSubject.next(null);
    return this.tokenService.getRefreshToken().pipe(
      switchMap((response: any) => {
        this.refreshTokenSubject.next(response.token);
        return next.handle(this.addToken(request, response.token));
      }),
      catchError((error) => {
        if (error instanceof HttpErrorResponse && error.status === 403) {
          localStorage.removeItem('token');
          this.router.navigate(['/login']);
        }
        return throwError(error);
      })
    );
  }
}
