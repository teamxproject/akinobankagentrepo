import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Observable, Subject } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { User } from '../../models/user';

@Injectable({
  providedIn: 'root',
})
export class ProfileService {
  host = environment.host;
  refreshHost = environment.refreshHost;
  agent: User;

  constructor(private http: HttpClient) {}

  // private _refreshNeeds$ = new Subject<void>();

  // get refreshNeeds$() {
  //   return this._refreshNeeds$;
  // }

  public getAgentProfile() {
    return this.http.get(this.host + '/profile');
  }

  public uploadImage(image: File) {
    //headers = headers.append("Authorization", "Bearer " +localStorage.getItem("token"))
    const data = new FormData();
    data.append('image', image);
    return this.http.post(this.host + '/profile/avatar/upload', data);
  }
  public downloadImage(imageName) {
    return this.http.get(this.host + '/profile/avatar/' + imageName, {
      responseType: 'blob',
    });
  }

  checkPassword(request: { password: string }): Observable<any> {
    return this.http.post(`${this.host}/profile/password_check`, request);
  }

  public deleteImage() {
    return this.http.delete(this.host + '/profile/avatar/delete/');
  }

  public changePassword(data) {
    return this.http.post(this.host + '/profile/modifier/password', data);
  }

  public modifyProfile(data) {
    return this.http.post(this.host + '/profile/modifier/user', data);
  }
  updateProfile(request: User): Observable<any> {
    return this.http.post(`${this.host}/profile/modifier/user`, request);
  }

  // public test(refresh_token:string){
  //   console.log('tst')
  //   const data = new FormData();
  //   data.append("refresh_token",refresh_token)
  //   this.http.post(this.refreshHost,data).subscribe(respone =>{
  //     console.log(respone);
  //   })
  // }
}
