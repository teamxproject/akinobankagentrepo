import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Subject, Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { User } from 'src/app/models/user';
import { Activity } from 'src/app/models/activity';

@Injectable({
  providedIn: 'root',
})
export class ClientsService {
  host = environment.host;
  constructor(private http: HttpClient) {}

  private _refreshNeeds$ = new Subject<void>();

  get refreshNeeds$() {
    return this._refreshNeeds$;
  }

  getAllClients(): Observable<User[]> {
    return this.http.get<User[]>(this.host + '/clients');
  }

  fetchClientActivities(id: number): Observable<Activity[]> {
    return this.http.get<Activity[]>(`${this.host}/clients/${id}/activities`);
  }

  public getClientImage(imageName) {
    return this.http.get(this.host + '/clients/avatar/' + imageName, {
      responseType: 'blob',
    });
  }
  addClient(client: User) {
    return this.http.post(this.host + '/clients/ajouter', client).pipe(
      tap(() => {
        this._refreshNeeds$.next();
      })
    );
  }
  getClientProfile(id: number): Observable<User> {
    return this.http.get(this.host + '/clients/' + id);
  }
  deleteClient(id: number) {
    return this.http.delete(this.host + '/clients/' + id + '/supprimer');
  }
  modifyClientSettings(id, client) {
    return this.http.put(this.host + '/clients/' + id + '/modifier', client);
  }
  modifyClientContacts(id, client) {
    return this.http.put(
      this.host + '/clients/' + id + '/modifier/contact',
      client
    );
  }
  sendClientVerification(id, clientData) {
    return this.http.post(
      this.host + '/clients/' + id + '/verification',
      clientData
    );
  }
  sendClientDataVerification(id, clientData) {
    return this.http.put(
      this.host + '/clients/' + id + '/modifier',
      clientData
    );
  }
}
