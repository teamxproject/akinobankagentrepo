import { Injectable } from '@angular/core';
import Stomp from 'stompjs';
import SockJs from 'sockjs-client';
import { TokenService } from './token.service';
import { CookieService } from './cookie.service';
import { StompConfig, StompRService, StompState } from '@stomp/ng2-stompjs';

@Injectable({
  providedIn: 'root',
})
export class WebsocketService {
  constructor(
    private tokenService: TokenService,
    private cookieService: CookieService,
    public stompService: StompRService
  ) {}

  public connect() {
    this.stompService.config = {
      headers: {
        Authorization: 'Bearer ' + this.tokenService.token,
        'X-XSRF-TOKEN': this.cookieService.get('XSRF-TOKEN'),
      },
      heartbeat_in: 30000,
      heartbeat_out: 30000,
      reconnect_delay: 5000,
      url: () => new SockJs('http://localhost:8080/ws'),
      debug: true,
    };
    this.stompService.initAndConnect();
  }

  public getStomp() {
    return this.stompService;
  }
}
