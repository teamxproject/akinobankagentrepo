import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class TokenService {
  constructor(private http: HttpClient) {}

  token = localStorage.getItem('token');

  getXSRFToken() {
    return this.http.get(
      `${environment.host.substr(0, environment.host.length - 9)}`,
      { withCredentials: true }
    );
  }

  getRefreshToken() {
    return this.http
      .post(`${environment.loginHost}/refresh`, {})
      .pipe(
        tap((response: any) => localStorage.setItem('token', response.token))
      );
  }

  isAuthenticated(): boolean {
    console.log('TOKEN', this.token);
    return this.token !== undefined && this.token !== null;
  }

  setToken(token: string) {
    localStorage.setItem('token', token);
    this.token = token;
  }

  unsetToken() {
    localStorage.removeItem('token');
    this.token = null;
  }
}
