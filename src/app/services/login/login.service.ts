import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../environments/environment";
import {Token} from "../../models/token";

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  public host = environment.loginHost;

  constructor(private http: HttpClient) { }

  public loginIn(user){
    return this.http.post(this.host ,user, { withCredentials: true });
  }
}
