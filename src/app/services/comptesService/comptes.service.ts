import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { Observable, Subject } from 'rxjs';
import { Compte } from 'src/app/models/compte';

@Injectable({
  providedIn: 'root',
})
export class ComptesService {
  host = environment.host;

  private _refreshNeeds$ = new Subject<void>();

  get refreshNeeds$() {
    return this._refreshNeeds$;
  }

  constructor(private http: HttpClient) {}

  addCompte(id: number, compte): Observable<Compte> {
    return this.http
      .post(this.host + '/clients/' + id + '/comptes/ajouter', compte)
      .pipe(
        tap(() => {
          this._refreshNeeds$.next();
        })
      );
  }

  getCompte(id) {
    return this.http.get(this.host + '/comptes/' + id);
  }
  modifyCompte(id, compte): Observable<Compte> {
    return this.http
      .put(this.host + '/comptes/' + id + '/modifier', compte)
      .pipe(
        tap(() => {
          this._refreshNeeds$.next();
        })
      );
  }
  modifyCompteStatus(id, status) {
    return this.http
      .put(
        this.host + '/comptes/' + id + '/modifier/status?status=' + status,
        {}
      )
      .pipe(
        tap(() => {
          this._refreshNeeds$.next();
        })
      );
  }
  deleteCompte(id): Observable<any> {
    return this.http.delete(this.host + '/comptes/' + id + '/supprimer');
  }

  resendInfo(id){
    return this.http.post(this.host +"/comptes/"+id+"/resend_infos",{});
  }
}
