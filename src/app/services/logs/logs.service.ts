import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {environment} from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class LogsService {

  host=environment.host;

  constructor(private http:HttpClient) { }

  getAllEvenets(page:any,size:any){
    let headers= new HttpHeaders();
    let data = new FormData();
    data.append("page",page);
    data.append("size",size);
    headers = headers.append("Authorization", "Bearer " +localStorage.getItem("token"))
    return this.http.post(this.host+"/profile/events",data,{headers})
  }
}
