import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class ForgetPasswordService {

  loginHost = environment.BASEURL
  constructor(private http:HttpClient) { }

  public forgetPassword(compteRequest){
    return this.http.post(this.loginHost+"/recover_account",compteRequest);
  }

}
