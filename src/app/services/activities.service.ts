import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Activity } from '../models/activity';

@Injectable({
  providedIn: 'root',
})
export class ActivitiesService {
  host = environment.host;
  refreshHost = environment.refreshHost;
  //   agent: User;

  constructor(private http: HttpClient) {}

  fetchClientsActivities(): Observable<Activity[]> {
    console.log("OPEN Clients activities")
    return this.http.get<Activity[]>(this.host+"/activities/clients");
  }

  fetchAgentActivities(): Observable<Activity[]> {
    return this.http.get<Activity[]>(`${this.host}/activities`);
  }
}
