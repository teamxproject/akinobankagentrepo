import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class DemandesService {

  host = environment.host;
  constructor(private http: HttpClient) { }

  public acceptDemande(numeroCompte){
    let data = new FormData();
    data.append('numeroCompte', numeroCompte);
    return this.http.post(this.host + "/demandes/accept",data);
  }




  public rejectDemende(numeroCompte){
    let data = new FormData();
    data.append('numeroCompte', numeroCompte);
    return this.http.post(this.host + "/demandes/reject",data);
  }
}
