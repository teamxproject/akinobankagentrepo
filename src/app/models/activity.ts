import { User } from './user';

export interface Activity {
  id?: number;
  evenement: string;
  category: string;
  timestamp: Date;
  user: User;
}
