export const environment = {
  production: true,
  host: '//localhost:8080/agent/api',
  clientHost: '//localhost:8080/client/api',
  loginHost: '//localhost:8080/api/auth',
  refreshHost: '//localhost:8080/api/refresh',
};
